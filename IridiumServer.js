var Alice = IR.GetDevice("Yandex Alice"); // имя драйвера Алисы      
var MQTT_DEVICE = IR.GetDevice("MQTT"); // MQTT драйвер
var answer = "Команда выполненна";

var ROOM_NAMES = {
    "спортзал": ["1_K2", "1_K4"],
    "прачку": ["2_Q6"],
    "лестницу": ["3_Q1"],
    "котельную": ["3_Q2"],
    "терассу": ["3_Q7"],
    "столовую": ["4_Q1", "4_Q2"],
    "кухня": ["4_Q3", "4_Q4"],
    "гостинную": ["4_Q5", "4_Q6", "4_Q7", "4_Q8"],
    "стол": ["4_Q2"],
    "люстру": ["4_Q6"],
    "бра телевизор": ["4_Q7"],
    "бра диван": ["4_Q8"],
    "коридор": ["5_Q1", "5_Q2"],
    "прихожую": ["5_Q4"],
    "гардероб": ["5_Q6"],
    "переход": ["5_Q8"],
    "кабинет": ["6_Q5"],
    "тамбур гараж": ["6_Q8"],
    "гараж": ["7_Q1", "7_Q2"],
    "никита": ["7_Q6"],
    "игровая": ["8_Q3"],
    "маша": ["8_Q5"],
    "варя": ["9_Q1", "9_Q4"],
    "игровая люстра": ["9_Q5"],
    "спальня телевизор": ["10_Q3"],
    "дом": ["10_Q6"],
    "дорожки": ["10_Q7"],
    "баня": ["10_Q8"],
    "щитовую":["3_Q5"]
}
var ONN_CMDS = ["включи ", "включить "];
var OFF_CMDS = ["выключи ", "выключить "];

function getRoomKey(prefix, deviceAnswer) {
    for (var key in ROOM_NAMES) {
        if (prefix + key == deviceAnswer) {
            return key
        }
    }
    return null;
}
function setCmdMqttDevice(keyRoom, setValue) {
    var room = ROOM_NAMES[keyRoom];
    if (!room) {
        return;
    }
    for (var i = 0; i < room.length; i++) {
        MQTT_DEVICE.Set(room[i], setValue);
    }
}

IR.AddListener(IR.EVENT_TAG_CHANGE, Alice, function (name, value) {
    if (name !== "Voice Command") {
        return;
    }

    var setDeviceValue = null;
    var keyRoom = null;
    var answer = "Команда не найдена";

    /** проверка на включение */
    for (var i = 0; i < ONN_CMDS.length; i++) {
        keyRoom = getRoomKey(ONN_CMDS[i], value);
        if (keyRoom != null) {
            setDeviceValue = 1;
            break;
        }
    }
    if (keyRoom != null && setDeviceValue != null) {
        setCmdMqttDevice(keyRoom, setDeviceValue);
        answer = "выполненно";
    }
    /**************************************** */

    /** проверка на выключение */
    for (var i = 0; i < OFF_CMDS.length; i++) {
        keyRoom = getRoomKey(OFF_CMDS[i], value);
        if (keyRoom != null) {
            setDeviceValue = 0;
            break;
        }
    }

    if (keyRoom != null && setDeviceValue != null) {
        setCmdMqttDevice(keyRoom, setDeviceValue);
        answer = "выполненно";
    }
    /******************************************** */

    switch (value) {
        case "включить спальню":
        case "включи спальню":
            MQTT_DEVICE.Set('10_Q2', 1);
            answer = "выполненно";
            break;
        case "выключить спальню":
        case "выключи спальню":
            MQTT_DEVICE.Set('badRommOff', 1);
            answer = "выполненно";
            break;
        case "выключить все":
        case "выключи всё":
            MQTT_DEVICE.Set('allOff', 1);
            answer = "выполненно";
            break;
        default:
            break;
    }
    Alice.Set("Alice Answer", answer);  // отправка ответа Алисе
})
