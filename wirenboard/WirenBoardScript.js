/*Дискретные устройства в системе*/
var myDiscretDevices = {
    'wb-mr6c_1': {
        Output: ['K1', 'K2', 'K3', 'K4', 'K5', 'K6'],
        Input: ['Input 0', 'Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6']
    },
    'ld2-r8d_2': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_3': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_4': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_5': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_6': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_7': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_8': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_9': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    },
    'ld2-r8d_10': {
        Output: ['Q1', 'Q2', 'Q3', 'Q4', 'Q5', 'Q6', 'Q7', 'Q8'],
        Input: ['Input 1', 'Input 2', 'Input 3', 'Input 4', 'Input 5', 'Input 6', 'Input 7', 'Input 8']
    }
};
/***************************/

/** Виртуальное утройство */
defineVirtualDevice("virtual-device_1", {
    title: "",
    cells: {
        allOff: {
            type: "switch",
            value: false
        },
        badRoomOff: {
            type: "switch",
            value: false
        },
    }
});
/********************************************************** */

/** Правила */

// таймер по нажатию
var TIMER_1_MS = 1000 * 3;
var motion_timer_1_id = null;
defineRule("test_rule", {
    whenChanged: "wb-mr6c_1/Input 0",
    then: function (newValue, devName, cellName) {  
        if (newValue && !motion_timer_1_id) {
            motion_timer_1_id = setTimeout(function () {
                dev["virtual-device_1"]["allOff"] = 1;
                motion_timer_1_id = null;
            }, TIMER_1_MS);
        }
        if (!newValue && motion_timer_1_id) {            
            clearTimeout(motion_timer_1_id);
            motion_timer_1_id = null;
        }
    }
});
/*********************************************/


/**Выключить весь свет */
defineRule("allOff", {
    whenChanged: "virtual-device_1/allOff",
    then: function (newValue, devName, cellName) {
        if (!newValue) {
            return;
        }
        for (var deviceName in myDiscretDevices) {
            if (myDiscretDevices.hasOwnProperty(deviceName)) {
                for (var index = 0; index < myDiscretDevices[deviceName].Output.length; index++) {
					if(dev[deviceName][myDiscretDevices[deviceName].Output[index]]){
						dev[deviceName][myDiscretDevices[deviceName].Output[index]] = 0;
					}
                }
            }

        }
        dev["virtual-device_1"]["allOff"] = 0;
    }
});

/**Выключить весь свет в спальне*/
defineRule("badRoomOff", {
    whenChanged: "virtual-device_1/badRoomOff",
    then: function (newValue, devName, cellName) {
        if (!newValue) {
            return;
        }
        dev["ld2-r8d_10"]["Q1"] = 0;
        dev["ld2-r8d_10"]["Q2"] = 0;
        dev["ld2-r8d_10"]["Q3"] = 0;
        dev["ld2-r8d_10"]["Q4"] = 0;
        dev["virtual-device_1"]["badRoomOff"] = 0;
    }
});